class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :post
      t.string :users
      t.string :category
      t.timestamps
    end
  end
end
