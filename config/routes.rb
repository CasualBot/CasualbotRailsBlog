Rails.application.routes.draw do
  resources :welcomes
  resources :posts do
    resources :comments
  end
  resources :users

  root 'welcomes#index'

end
